import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';

import {
  ReactionInfo,
  UserInfo,
} from '@components';

class Card extends PureComponent {
  render() {
    const {
      data,
      openFullscreen,
    } = this.props;
    const imageUrl = get(data, 'images.480w_still.url', '');
    const reactionInfoData = {
      isAttachment: Math.floor((Math.random() * 10) + 1) >= 8,
      view: Math.floor((Math.random() * 6500) + 700),
      comment: Math.floor((Math.random() * 100) + 1),
      like: Math.floor((Math.random() * 1000) + 1),
    };
    const userInfoData = {
      avatarUrl: get(data, 'user.avatar_url', ''),
      userDisplayName: get(data, 'user.display_name', ''),
      userProfileUrl: get(data, 'user.profile_url', ''),
    };

    return (
      <div className="card">
        <div className="card-preview">
          <div
            className="card-preview__image"
            style={{
              backgroundImage: `url(${imageUrl})`,
              cursor: 'pointer'
            }}
            onClick={openFullscreen}
            onKeyDown={() => null}
          />
          <div className="card-preview__info">
            <ReactionInfo data={reactionInfoData} />
          </div>
        </div>
        <div className="card-user">
          <UserInfo data={userInfoData} />
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  data: PropTypes.object,
  openFullscreen: PropTypes.func,
};

export default Card;
