import React from 'react';
// import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Header = () => (
  <header className="header">
    <h1 className="g-title">ReactJs Test for MessageMedia</h1>
    <div className="header-action">
      <div className="row">
        <div className="col-6">
          <Link to="/about" href="/about">
            <button className="btn btn-info">About</button>
          </Link>
        </div>

        <div className="col-6">
          <a href="https://bitbucket.org/nic269/mm-test" target="_blank" rel="noopener noreferrer">
            <button className="btn btn-danger">Source</button>
          </a>
        </div>
      </div>
    </div>
  </header>
);

Header.propTypes = {

};

export default Header;
