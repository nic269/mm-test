import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import get from 'lodash.get';

import {
  ReactionInfo,
  UserInfo,
} from '@components';

class FullScreen extends PureComponent {
  render() {
    const {
      isOpenModal,
      toogleModal,
      data,
    } = this.props;
    const imageUrl = get(data, 'images.original.url', '');
    const reactionInfoData = {
      isAttachment: Math.floor((Math.random() * 10) + 1) >= 8,
      view: Math.floor((Math.random() * 6500) + 700),
      comment: Math.floor((Math.random() * 100) + 1),
      like: Math.floor((Math.random() * 1000) + 1),
    };
    const userInfoData = {
      avatarUrl: get(data, 'user.avatar_url', ''),
      userDisplayName: get(data, 'user.display_name', ''),
      userProfileUrl: get(data, 'user.profile_url', ''),
    };

    return (
      <Modal
        open={isOpenModal}
        onClose={toogleModal}
        little
        styles={{
          closeIcon: {
            fill: '#ed1b2e',
            zIndex: 1,
          },
          modal: {
            padding: 0,
            width: '100vw',
            height: '100vh',
            maxWidth: 'none',
            backgroundColor: '#000'
          },
          overlay: {
            padding: 0
          }
        }}
      >
        <div className="fullscreen">
          <div className="fullscreen-top">
            <UserInfo data={userInfoData} />
          </div>
          <img src={imageUrl} alt="" className="fullscreen__img" />
          <div className="fullscreen-bottom">
            <ReactionInfo data={reactionInfoData} />
          </div>
        </div>
      </Modal>
    );
  }
}

FullScreen.propTypes = {
  toogleModal: PropTypes.func,
  isOpenModal: PropTypes.bool,
  data: PropTypes.object,
};

export default FullScreen;
