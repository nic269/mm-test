import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Loading } from '@components';

class Loadmore extends PureComponent {
  render() {
    const {
      doLoadmore,
      onLoading,
      moreitem,
    } = this.props;

    return (
      <div className="loadmore">
        <button
          className="btn btn-danger"
          onClick={doLoadmore}
          onKeyPress={doLoadmore}
          disabled={onLoading}
        >
          {
            !onLoading &&
            <span>Load more ({moreitem})</span>
          }
          {
            onLoading &&
            <React.Fragment>
              <span>Loading ...</span>
              <Loading size="small" inline />
            </React.Fragment>
          }
        </button>
      </div>
    );
  }
}

Loadmore.propTypes = {
  doLoadmore: PropTypes.func,
  onLoading: PropTypes.bool,
  moreitem: PropTypes.number,
};

export default Loadmore;
