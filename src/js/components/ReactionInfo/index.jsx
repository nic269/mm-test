import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';

import { numberWithComma } from '@utils/helpers';

class Reactioninfo extends PureComponent {
  render() {
    const { data } = this.props;
    const view = get(data, 'view', 0);
    const comment = get(data, 'comment', 0);
    const like = get(data, 'like', 0);
    const isAttachment = get(data, 'isAttachment', false);

    return (
      <div className="reaction-info">
        {
          isAttachment &&
          <div className="attachment">
            <i className="icon icon-paperclip" />
          </div>
        }
        <div className="reaction-info__stats">
          <div className="stats-item">
            <i className="icon icon-eye" />
            <span>{numberWithComma(view)}</span>
          </div>

          <div className="stats-item">
            <i className="icon icon-comment" />
            <span>{numberWithComma(comment)}</span>
          </div>

          <div className="stats-item">
            <i className="icon icon-heart" />
            <span>{numberWithComma(like)}</span>
          </div>
        </div>
      </div>
    );
  }
}

Reactioninfo.propTypes = {
  data: PropTypes.object,
};

export default Reactioninfo;
