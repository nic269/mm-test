import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';

class UserInfo extends PureComponent {
  render() {
    const { data } = this.props;
    const avatarUrl = get(data, 'avatarUrl', '');
    const userDisplayName = get(data, 'userDisplayName', '');
    const userProfileUrl = get(data, 'userProfileUrl', '');

    return (
      <div className="user-info">
        <div className="user-info__avatar">
          <img src={avatarUrl} alt={userDisplayName} />
        </div>
        <div className="user-info__name">
          <a href={userProfileUrl} target="_blank">{userDisplayName}</a>
        </div>
      </div>
    );
  }
}

UserInfo.propTypes = {
  data: PropTypes.object,
};

export default UserInfo;
