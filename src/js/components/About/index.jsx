import React from 'react';
import { Link } from 'react-router-dom';

const About = () => (
  <div className="about">
    <div className="container">
      <h2 className="g-title">
        ReactJs Test for MessageMedia
      </h2>
      <p className="about__desc">
        A simple web app show image. Data get from https://api.giphy.com/v1/gifs/trending.
      </p>
      <p className="about__copyright">
        Design by Tuan Anh (tuananh.exp@gmail.com)
      </p>
      <Link to="/" href="/">
        <button className="btn btn-danger">Back to home</button>
      </Link>
    </div>
  </div>
);

export default About;
