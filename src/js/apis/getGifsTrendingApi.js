import { request } from '@utils/helpers';
import { API_ACCESS_KEY, API_END_POINT } from '@utils/constants';

export const getGifsTrendingApi = ({ limit, offset }) => request(
  `${API_END_POINT}/trending`,
  {
    method: 'GET',
    params: {
      api_key: API_ACCESS_KEY,
      limit,
      offset,
      rating: 'G',
    },
  },
);
