import { fromJS } from 'immutable';
import get from 'lodash.get';

import { GET_GIFS_TRENDING, LOAD_MORE_GIFS } from './constants';

const initialState = fromJS({
  gifsTrending: {
    loading: false,
    data: null,
    error: null,
  },
  moreGifs: {
    loading: false,
    error: null
  }
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case GET_GIFS_TRENDING.REQUEST:
      return state
        .setIn(['gifsTrending', 'loading'], true)
        .setIn(['gifsTrending', 'data'], null)
        .setIn(['gifsTrending', 'error'], null);
    case GET_GIFS_TRENDING.SUCCESS:
      return state
        .setIn(['gifsTrending', 'loading'], false)
        .setIn(['gifsTrending', 'data'], action.data)
        .setIn(['gifsTrending', 'error'], null);
    case GET_GIFS_TRENDING.FAILURE:
      return state
        .setIn(['gifsTrending', 'loading'], false)
        .setIn(['gifsTrending', 'data'], null)
        .setIn(['gifsTrending', 'error'], action.error);

    case LOAD_MORE_GIFS.REQUEST:
      return state
        .setIn(['moreGifs', 'loading'], true)
        .setIn(['moreGifs', 'error'], null);
    case LOAD_MORE_GIFS.SUCCESS:
      return state
        .setIn(['moreGifs', 'loading'], false)
        .updateIn(['gifsTrending', 'data'], (gifsTrendingData) => {
          const currentData = get(gifsTrendingData, 'data', []);
          const updatingData = get(action, 'data.data', []);

          return {
            ...gifsTrendingData,
            ...action.data,
            data: [...currentData, ...updatingData],
          };
        })
        .setIn(['moreGifs', 'error'], null);
    case LOAD_MORE_GIFS.FAILURE:
      return state
        .setIn(['moreGifs', 'loading'], false)
        .setIn(['moreGifs', 'error'], action.error);
    default:
      return state;
  }
}

export default homePageReducer;
