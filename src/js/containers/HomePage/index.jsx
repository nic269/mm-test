import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import get from 'lodash.get';

import {
  Loading,
  Card,
  FullScreen,
  Header,
  Loadmore,
} from '@components';
import withMessageNotifyHandler from '@HOC/withMessageNotifyHandler';

import {
  getGifsTrendingRequest,
  loadmoreGifsRequest,
} from './actions';
import {
  makeSelectGifsTrending,
  makeSelectMoreGifs,
} from './selectors';
import { GET_GIFS_OPTIONS } from './constants';

class HomeContainer extends PureComponent {
  state = {
    modalOpenedId: '',
    pagination: {
      currentPage: 0,
      offset: 0,
    },
  }

  componentDidMount() {
    this.getGifsData({ isInitial: true });
  }

  componentDidUpdate(prevProps) {
    const {
      gifsTrendingData,
      gifsTrendingError,
      moreGifsError,
    } = this.props;
    const {
      gifsTrendingLoading: prevGifsTrendingLoading,
      loadmoreGifsRequest: prevloadmoreGifsRequest,
    } = prevProps;

    if (prevGifsTrendingLoading) {
      if (gifsTrendingError) {
        const error = get(gifsTrendingError, 'response.data.meta', null);

        this.props.showError(`${error.status}: ${error.msg}`);
      } else if (gifsTrendingData && gifsTrendingData.error) {
        const error = get(gifsTrendingData, 'meta', null);

        this.props.showError(`${error.status}: ${error.msg}`);
      }
    }

    if (prevloadmoreGifsRequest) {
      if (moreGifsError) {
        const error = get(moreGifsError, 'response.data.meta', null);

        this.props.showError(`${error.status}: ${error.msg}`);
      } else if (gifsTrendingData && gifsTrendingData.error) {
        const error = get(gifsTrendingData, 'meta', null);

        this.props.showError(`${error.status}: ${error.msg}`);
      }
    }
  }

  getGifsData = ({ isInitial = false } = {}) => {
    const fetchFunc = isInitial ? this.props.getGifsTrendingRequest : this.props.loadmoreGifsRequest;
    const limit = isInitial ? GET_GIFS_OPTIONS.limit : GET_GIFS_OPTIONS.moreItem;

    fetchFunc({
      limit,
      offset: this.state.pagination.offset,
    });
  }

  toogleModal = (modalId) => {
    if (modalId === this.state.modalOpenedId) {
      this.setState({
        modalOpenedId: '',
      });
    } else {
      this.setState({
        modalOpenedId: modalId,
      });
    }
  }

  loadmore = () => {
    const nextPage = this.state.pagination.currentPage + 1;
    const currentItem = get(this.props, 'gifsTrendingData.data.length', 0);

    this.setState({
      pagination: {
        currentPage: nextPage,
        offset: currentItem,
      },
    }, () => {
      this.getGifsData();
    });
  }

  render() {
    const {
      gifsTrendingLoading,
      gifsTrendingData,
      moreGifsLoading,
    } = this.props;
    const gifs = get(gifsTrendingData, 'data', []);
    const itemLoaded = gifs.length;
    const totalItem = get(gifsTrendingData, 'pagination.total_count', 0);

    if (gifsTrendingLoading) {
      return <Loading />;
    }

    return (
      <React.Fragment>
        <Header />
        <div className="main">
          <div className="row">
            {
              !!gifs.length &&
              gifs.map((gif, idx) => (
                <div className="col-lg-3 col-md-4 col-6" key={`${gif.id}-${idx}`}>
                  <Card data={gif} openFullscreen={() => this.toogleModal(gif.id)} />
                  <FullScreen
                    isOpenModal={this.state.modalOpenedId === gif.id}
                    toogleModal={() => this.toogleModal(gif.id)}
                    data={gif}
                  />
                </div>
              ))
            }
            {
              itemLoaded < totalItem &&
              <Loadmore
                doLoadmore={this.loadmore}
                onLoading={moreGifsLoading}
                moreitem={GET_GIFS_OPTIONS.moreItem}
              />
            }
          </div>
        </div>
      </React.Fragment>
    );
  }
}

HomeContainer.propTypes = {
  getGifsTrendingRequest: PropTypes.func,
  loadmoreGifsRequest: PropTypes.func,
  gifsTrendingData: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
  ]),
  gifsTrendingError: PropTypes.object,
  gifsTrendingLoading: PropTypes.bool,
  moreGifsError: PropTypes.object,
  moreGifsLoading: PropTypes.bool,
  showError: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  gifsTrendingData: makeSelectGifsTrending.data(),
  gifsTrendingError: makeSelectGifsTrending.error(),
  gifsTrendingLoading: makeSelectGifsTrending.loading(),
  moreGifsError: makeSelectMoreGifs.error(),
  moreGifsLoading: makeSelectMoreGifs.loading(),
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    getGifsTrendingRequest,
    loadmoreGifsRequest,
  }, dispatch)
});

const errorHandlerOptions = {
  position: 'bottom-right',
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  draggablePercent: 50,
  closeButton: false,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withMessageNotifyHandling = withMessageNotifyHandler(errorHandlerOptions);

export default compose(
  withMessageNotifyHandling,
  withConnect,
)(HomeContainer);
