import { call, put, takeLatest } from 'redux-saga/effects';

import { getGifsTrendingApi } from '@apis/getGifsTrendingApi';
import { GET_GIFS_TRENDING, LOAD_MORE_GIFS } from './constants';
import {
  getGifsTrendingSuccess,
  getGifsTrendingFail,
  loadmoreGifsSuccess,
  loadmoreGifsFail,
} from './actions';

// getGifsTrending
function* getGifsTrending({ params }) {
  try {
    const response = yield call(getGifsTrendingApi, params);
    yield put(getGifsTrendingSuccess(response));
  } catch (error) {
    yield put(getGifsTrendingFail(error));
  }
}

// loadmoreGifs
function* loadmoreGifs({ params }) {
  try {
    const response = yield call(getGifsTrendingApi, params);
    yield put(loadmoreGifsSuccess(response));
  } catch (error) {
    yield put(loadmoreGifsFail(error));
  }
}

export default function* homePageSaga() {
  yield takeLatest(GET_GIFS_TRENDING.REQUEST, getGifsTrending);
  yield takeLatest(LOAD_MORE_GIFS.REQUEST, loadmoreGifs);
}
