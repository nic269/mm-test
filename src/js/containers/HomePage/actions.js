import { GET_GIFS_TRENDING, LOAD_MORE_GIFS } from './constants';

export const getGifsTrendingRequest = params => ({ type: GET_GIFS_TRENDING.REQUEST, params });
export const getGifsTrendingSuccess = data => ({ type: GET_GIFS_TRENDING.SUCCESS, data });
export const getGifsTrendingFail = error => ({ type: GET_GIFS_TRENDING.FAILURE, error });

export const loadmoreGifsRequest = params => ({ type: LOAD_MORE_GIFS.REQUEST, params });
export const loadmoreGifsSuccess = data => ({ type: LOAD_MORE_GIFS.SUCCESS, data });
export const loadmoreGifsFail = error => ({ type: LOAD_MORE_GIFS.FAILURE, error });
