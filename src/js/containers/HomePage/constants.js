import { createActionType } from '@utils/helpers';

export const GET_GIFS_TRENDING = createActionType('GET_GIFS_TRENDING', '@homepage');
export const LOAD_MORE_GIFS = createActionType('LOAD_MORE_GIFS', '@homepage');

export const GET_GIFS_OPTIONS = {
  limit: 20,
  moreItem: 20,
};
