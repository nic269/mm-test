import { createSelector } from 'reselect';

const selectHomePage = state => state.get('homePageReducer');

const makeSelectGifsTrendingData = () => createSelector(
  selectHomePage,
  homePageState => homePageState.getIn(['gifsTrending', 'data']),
);

const makeSelectGifsTrendingLoading = () => createSelector(
  selectHomePage,
  homePageState => homePageState.getIn(['gifsTrending', 'loading']),
);

const makeSelectGifsTrendingError = () => createSelector(
  selectHomePage,
  homePageState => homePageState.getIn(['gifsTrending', 'error']),
);

export const makeSelectGifsTrending = {
  data: makeSelectGifsTrendingData,
  loading: makeSelectGifsTrendingLoading,
  error: makeSelectGifsTrendingError,
};

const makeSelectMoreGifsLoading = () => createSelector(
  selectHomePage,
  homePageState => homePageState.getIn(['moreGifs', 'loading']),
);

const makeSelectMoreGifsError = () => createSelector(
  selectHomePage,
  homePageState => homePageState.getIn(['moreGifs', 'error']),
);

export const makeSelectMoreGifs = {
  loading: makeSelectMoreGifsLoading,
  error: makeSelectMoreGifsError,
};

export default {
  makeSelectGifsTrendingData,
  makeSelectGifsTrendingLoading,
  makeSelectGifsTrendingError,
  makeSelectMoreGifsLoading,
  makeSelectMoreGifsError,
};
