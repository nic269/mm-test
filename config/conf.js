const path = require('path');

const resolvePath = dir => path.join(__dirname, '..', dir);
const directory = {
  SRC: resolvePath('src'),
  DIST: resolvePath('public'),
};
const HTMLTemplateSettings = {
  title: 'ReactJs Test for MessageMedia',
  og: {
    title: 'ReactJs Test for MessageMedia',
    description: 'Build my own react-app boilerplate from scratch. A simple web app show image. Data get from https://api.giphy.com/v1/gifs/trending. Used react, redux, redux-saga, webpack 4',
    image: '',
  },
};
const region = process.env.REGION || 'en';
const port = process.env.PORT || 3000;

module.exports = {
  resolvePath,
  directory,
  HTMLTemplateSettings,
  region,
  port,
};
